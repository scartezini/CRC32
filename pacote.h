#include <stdlib.h>
#include <stdio.h>

// Substituir este valor pelo tamanho do pacote
#define TAM_PACOTE 520

// Pacote 6904 do arquivo pacotes_projeto1.pcapng, 520 bytes, 4160 bits
unsigned char pkt6904[520] = {
  0x0c, 0xee, 0xe6, 0x94, 0x8f, 0xe5, 0x00, 0x1a, /* ........ */
  0x1e, 0x02, 0x38, 0x10, 0x08, 0x00, 0x45, 0x00, /* ..8...E. */
  0x01, 0xfa, 0x3e, 0x2f, 0x40, 0x00, 0x2c, 0x06, /* ..>/@.,. */
  0x04, 0x88, 0xd4, 0xe3, 0xaa, 0x6b, 0xac, 0x1f, /* .....k.. */
  0xde, 0xd8, 0x00, 0x50, 0xa3, 0x86, 0xc8, 0x81, /* ...P.... */
  0x71, 0xcf, 0x2b, 0xe7, 0xdb, 0x04, 0x80, 0x18, /* q.+..... */
  0x01, 0x75, 0x04, 0x5d, 0x00, 0x00, 0x01, 0x01, /* .u.].... */
  0x08, 0x0a, 0x23, 0x22, 0xac, 0x9c, 0x00, 0x07, /* ..#".... */
  0xed, 0xb5, 0x46, 0xdf, 0x1a, 0xf8, 0x89, 0x47, /* ..F....G */
  0x9e, 0x7e, 0xd6, 0x75, 0xb7, 0x5d, 0x79, 0xd1, /* .~.u.]y. */
  0x11, 0x48, 0x60, 0x81, 0x03, 0x02, 0x58, 0xe0, /* .H`...X. */
  0x85, 0xbf, 0x25, 0xb8, 0xde, 0x70, 0x91, 0x35, /* ..%..p.5 */
  0xb8, 0x1f, 0x84, 0xc7, 0x71, 0x47, 0x1f, 0x86, /* ....qG.. */
  0x22, 0xf6, 0x77, 0xdf, 0x6b, 0x2b, 0xfa, 0x67, /* ".w.k+.g */
  0x23, 0x88, 0xba, 0xd5, 0xf8, 0xe1, 0x65, 0x26, /* #.....e& */
  0x9e, 0x18, 0x56, 0x8a, 0x0c, 0x4d, 0x27, 0xa4, /* ..V..M'. */
  0x41, 0xef, 0xad, 0x71, 0x9c, 0x80, 0x0f, 0x89, /* A..q.... */
  0xb8, 0x61, 0x76, 0x48, 0x86, 0xe7, 0x9d, 0x90, /* .avH.... */
  0x00, 0x2a, 0xa9, 0xe4, 0x76, 0x15, 0x41, 0x29, /* .*..v.A) */
  0xde, 0x94, 0x8b, 0xf5, 0xe8, 0x63, 0x67, 0x40, /* .....cg@ */
  0x26, 0x46, 0xe1, 0x81, 0x60, 0x2e, 0xa5, 0xe5, /* &F..`... */
  0x96, 0xed, 0x79, 0x19, 0xe6, 0x99, 0x4a, 0x8d, /* ..y...J. */
  0xa9, 0x20, 0x7b, 0xc4, 0x99, 0x89, 0xe6, 0x9b, /* . {..... */
  0x6b, 0xa9, 0xd9, 0xd4, 0x82, 0x9f, 0x95, 0x05, /* k....... */
  0xe7, 0x9d, 0x39, 0xc9, 0x39, 0x27, 0x9b, 0x5d, /* ..9.9'.] */
  0x16, 0x85, 0xe7, 0x9f, 0x18, 0xe9, 0xb9, 0x26, /* .......& */
  0x8a, 0x0c, 0xfe, 0x05, 0xe8, 0xa1, 0x13, 0x09, /* ........ */
  0x3a, 0xe8, 0x8f, 0x85, 0x0e, 0x85, 0x68, 0x44, /* :.....hD */
  0xf9, 0xf1, 0x96, 0x54, 0x41, 0x58, 0x29, 0xaa, /* ...TAX). */
  0xe0, 0x0d, 0x84, 0xd6, 0xf9, 0xd2, 0xa3, 0xba, /* ........ */
  0xc9, 0xc7, 0x64, 0x6d, 0x01, 0xe2, 0xc7, 0x9b, /* ..dm.... */
  0x8c, 0xa4, 0xd2, 0x06, 0xde, 0x19, 0x1e, 0xc1, /* ........ */
  0xd3, 0x91, 0xaa, 0x1b, 0xb1, 0xba, 0x9b, 0x46, /* .......F */
  0xfd, 0xaf, 0x6a, 0xb1, 0x1f, 0xac, 0x1c, 0xb1, /* ..j..... */
  0xba, 0x91, 0x15, 0x98, 0x32, 0xaa, 0x29, 0x43, /* ....2.)C */
  0x9c, 0xe6, 0xa8, 0x61, 0x80, 0xf9, 0x8d, 0x68, /* ...a...h */
  0x25, 0x76, 0x1d, 0x0a, 0x69, 0xde, 0xb0, 0xa1, /* %v..i... */
  0xa5, 0x2a, 0x6b, 0xad, 0x00, 0xb4, 0xda, 0xac, /* .*k..... */
  0x16, 0x2e, 0x42, 0x27, 0x2b, 0x94, 0xd3, 0x72, /* ..B'+..r */
  0x28, 0xab, 0x6a, 0xb0, 0x3e, 0xab, 0x11, 0x9d, /* (.j.>... */
  0xc5, 0x35, 0xd4, 0xab, 0xaf, 0x53, 0x4a, 0x88, /* .5...SJ. */
  0x9b, 0x76, 0xfb, 0xcd, 0x36, 0xde, 0x6e, 0x1d, /* .v..6.n. */
  0xca, 0xa8, 0x2d, 0xac, 0xa1, 0x61, 0x9b, 0xad, /* ..-..a.. */
  0xb3, 0xf0, 0xb2, 0x6b, 0xeb, 0xb5, 0xb1, 0x35, /* ...k...5 */
  0xdb, 0xae, 0xaa, 0xc9, 0xde, 0xca, 0x67, 0xa3, /* ......g. */
  0x67, 0x7d, 0xdb, 0xa9, 0x7c, 0xe7, 0xba, 0x06, /* g}..|... */
  0xe0, 0x7d, 0xc6, 0x16, 0x8b, 0x5e, 0x77, 0x12, /* .}...^w. */
  0x46, 0xc9, 0xe1, 0x73, 0xab, 0xd6, 0xfb, 0xee, /* F..s.... */
  0xc3, 0xd3, 0x3a, 0x2b, 0x6d, 0xb6, 0x9e, 0xda, /* ..:+m... */
  0x6b, 0xaf, 0x84, 0x5f, 0xed, 0xbb, 0xeb, 0x97, /* k.._.... */
  0xfe, 0x86, 0x68, 0x6e, 0xc2, 0x00, 0x93, 0x8b, /* ..hn.... */
  0xae, 0xa8, 0x91, 0x0e, 0x7c, 0xdb, 0xc8, 0xaf, /* ....|... */
  0x82, 0xec, 0xee, 0x8a, 0xf1, 0xd2, 0xbb, 0xf2, /* ........ */
  0xb3, 0xae, 0x45, 0x0c, 0x6d, 0xbe, 0xdb, 0x6a, /* ..E.m..j */
  0x5c, 0x5c, 0xc7, 0x17, 0x89, 0x98, 0x1d, 0x7a, /* \\.....z */
  0x57, 0x2e, 0xc9, 0x24, 0x94, 0x54, 0x2e, 0xe9, /* W..$.T.. */
  0xa4, 0x90, 0x1a, 0x51, 0x3b, 0x31, 0xb4, 0xcf, /* ...Q;1.. */
  0x5a, 0xc9, 0xd1, 0xba, 0xd4, 0x42, 0xbb, 0xd1, /* Z....B.. */
  0x74, 0x4e, 0x3f, 0x9d, 0x71, 0xa6, 0x64, 0xe1, /* tN?.q.d. */
  0xdc, 0xb1, 0xa5, 0x79, 0x71, 0xcb, 0xb1, 0xd5, /* ...yq... */
  0xbd, 0x62, 0x9d, 0xb5, 0xcd, 0x5c, 0xfb, 0xeb, /* .b...\.. */
  0xf5, 0xd7, 0x54, 0x87, 0xdd, 0xf5, 0xd8, 0x6d, /* ..T....m */
  0x19, 0x66, 0xe5, 0xda, 0x6c, 0xb7, 0xed, 0xf6, /* .f..l... */
  0xdb, 0x70, 0xc7, 0x2d, 0xf7, 0xdc, 0x74, 0xd7, /* .p.-..t. */
  0x6d, 0xf7, 0xdd, 0x76, 0x07, 0x04, 0x00, 0x3b  /* m..v...; */
};

// Campo CRC calculado pelo Wireshark = 0xeee97d9e
